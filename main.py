from index import app
from flask import flash, render_template, request, redirect, jsonify, url_for
from flaskext.mysql import MySQL 



#DATABASE CONFIGURATIONS
app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = 'admin'
app.config['MYSQL_DATABASE_DB'] = 'pythondev'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'

mysql = MySQL(app)
mysql.init_app(app)


@app.route('/add', methods=['POST', 'GET'])
def add_user_view():
    if request.method == 'POST':
        userDetails = request.form
        name = userDetails['inputName']
        cur = mysql.get_db().cursor()
        cur.execute("INSERT INTO User(name) VALUES(%s)", (name))
        mysql.get_db().commit()
        cur.close()
        return render_template('success.html')

    if request.method == 'GET':
       return render_template('add.html')


@app.route('/users', methods=['GET'])
def getUsers():
    try:
        cur = mysql.get_db().cursor()
        cur.execute("SELECT * FROM pythondev.User;")
        rows = cur.fetchall()
        resp = jsonify(rows)
        return resp
    except Exception as e:
        print(e)
    finally:
        cur.close()
  

app.run(debug=True)   